" ===============================================================
" calmdown
" 
" URL: 
" Author: Andrey Parfenov
" License: MIT
" Last Change: 2020/05/03 18:18
" ===============================================================

set background=dark
hi clear
if exists("syntax_on")
  syntax reset
endif
let g:colors_name="calmdown"


let Italic = ""
if exists('g:calmdown_italic')
  let Italic = "italic"
endif
let g:calmdown_italic = get(g:, 'calmdown_italic', 0)

let Bold = ""
if exists('g:calmdown_bold')
  let Bold = "bold"
endif

let g:calmdown_bold = get(g:, 'calmdown_bold', 0)
hi ColorColumn guibg=#a6a7a5 ctermbg=248 gui=NONE cterm=NONE
hi Conceal guifg=#403f4c ctermfg=238 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Cursor guibg=#e5e6e4 ctermbg=254 gui=NONE cterm=NONE
hi CursorIM guifg=#403f4c ctermfg=238 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi CursorColumn guifg=NONE ctermfg=NONE guibg=#cad2c5 ctermbg=251 gui=NONE cterm=NONE
hi CursorLine guifg=NONE ctermfg=NONE guibg=#cad2c5 ctermbg=251 gui=NONE cterm=NONE
hi Directory guifg=#1d0f0b ctermfg=233 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi DiffAdd guifg=NONE ctermfg=NONE guibg=#1b264f ctermbg=235 gui=NONE cterm=NONE
hi DiffChange guifg=NONE ctermfg=NONE guibg=#403f4c ctermbg=238 gui=NONE cterm=NONE
hi DiffDelete guifg=NONE ctermfg=NONE guibg=#d62828 ctermbg=160 gui=NONE cterm=NONE
hi DiffText guifg=NONE ctermfg=NONE guibg=#1b264f ctermbg=235 gui=NONE cterm=NONE
hi ErrorMsg guifg=#d62828 ctermfg=160 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi VertSplit guifg=#f1faee ctermfg=255 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Folded guifg=#1d0f0b ctermfg=233 guibg=#a6a7a5 ctermbg=248 gui=NONE cterm=NONE
hi FoldColumn guifg=#1d0f0b ctermfg=233 guibg=#a6a7a5 ctermbg=248 gui=NONE cterm=NONE
hi SignColumn guifg=#1d0f0b ctermfg=233 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi IncSearch guifg=#a6a7a5 ctermfg=248 guibg=#e5e6e4 ctermbg=254 gui=NONE cterm=NONE
hi LineNr guifg=#333533 ctermfg=236 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi MatchParen guifg=#333533 ctermfg=236 guibg=#cad2c5 ctermbg=251 gui=Bold cterm=Bold
hi ModeMsg guifg=#403f4c ctermfg=238 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi MoreMsg guifg=#403f4c ctermfg=238 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi NonText guifg=#403f4c ctermfg=238 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Normal guifg=#1d0f0b ctermfg=233 guibg=#a6a7a5 ctermbg=248 gui=NONE cterm=NONE
hi PMenu guifg=#595959 ctermfg=240 guibg=#e5e6e4 ctermbg=254 gui=NONE cterm=NONE
hi PMenuSel guifg=#cad2c5 ctermfg=251 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi SpecialKey guifg=#403f4c ctermfg=238 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi TabLine guifg=#403f4c ctermfg=238 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi TabLineFill guifg=#403f4c ctermfg=238 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi TabLineSel guifg=#403f4c ctermfg=238 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Title guifg=#595959 ctermfg=240 guibg=NONE ctermbg=NONE gui=Bold cterm=Bold
hi Visual guifg=#1d0f0b ctermfg=233 guibg=#cad2c5 ctermbg=251 gui=NONE cterm=NONE
hi WarningMsg guifg=#403f4c ctermfg=238 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi WildMenu guifg=#403f4c ctermfg=238 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Comment guifg=#1b264f ctermfg=235 guibg=NONE ctermbg=NONE gui=Italic cterm=Italic
hi Constant guifg=#1d0f0b ctermfg=233 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi String guifg=#333533 ctermfg=236 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Character guifg=#1d0f0b ctermfg=233 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Boolean guifg=#1d0f0b ctermfg=233 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Number guifg=#403f4c ctermfg=238 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Float guifg=#403f4c ctermfg=238 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Identifier guifg=#1d0f0b ctermfg=233 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Function guifg=#333533 ctermfg=236 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Statement guifg=#1d0f0b ctermfg=233 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Conditional guifg=#1d0f0b ctermfg=233 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Repeat guifg=#1d0f0b ctermfg=233 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Label guifg=#1d0f0b ctermfg=233 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Operator guifg=#1d0f0b ctermfg=233 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Keyword guifg=#1d0f0b ctermfg=233 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Exception guifg=#1d0f0b ctermfg=233 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi PreProc guifg=#1d0f0b ctermfg=233 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Include guifg=#1d0f0b ctermfg=233 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Define guifg=#1d0f0b ctermfg=233 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Macro guifg=#1d0f0b ctermfg=233 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi PreCondit guifg=#1d0f0b ctermfg=233 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Type guifg=#333533 ctermfg=236 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi StorageClass guifg=#1d0f0b ctermfg=233 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Structure guifg=#1d0f0b ctermfg=233 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Typedef guifg=#1d0f0b ctermfg=233 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Special guifg=#1d0f0b ctermfg=233 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi SpecialChar guifg=#1d0f0b ctermfg=233 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Tag guifg=#1d0f0b ctermfg=233 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Delimiter guifg=#403f4c ctermfg=238 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi SpecialComment guifg=#1b264f ctermfg=235 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Debug guifg=#1d0f0b ctermfg=233 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Underlined guifg=#333533 ctermfg=236 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Ignore guifg=#1d0f0b ctermfg=233 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Error guifg=#d62828 ctermfg=160 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Todo guifg=#e5e6e4 ctermfg=254 guibg=#1d0f0b ctermbg=233 gui=NONE cterm=NONE
hi javaScriptFunction guifg=#1d0f0b ctermfg=233 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE

if exists('*term_setansicolors')
  let g:terminal_ansi_colors = repeat([0], 16)

endif

if has('nvim')
endif

" ===================================
" Generated by Estilo 1.5.0
" https://github.com/jacoborus/estilo
" ===================================
