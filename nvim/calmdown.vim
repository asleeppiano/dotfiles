" ===============================================================
" calmdown
" 
" URL: 
" Author: Andrey Parfenov
" License: MIT
" Last Change: 2020/05/03 17:30
" ===============================================================

set background=dark
hi clear
if exists("syntax_on")
  syntax reset
endif
let g:colors_name="calmdown"


let Italic = ""
if exists('g:calmdown_italic')
  let Italic = "italic"
endif
let g:calmdown_italic = get(g:, 'calmdown_italic', 0)

let Bold = ""
if exists('g:calmdown_bold')
  let Bold = "bold"
endif

let g:calmdown_bold = get(g:, 'calmdown_bold', 0)
hi ColorColumn guibg=#a6a7a5 ctermbg=248 gui=NONE cterm=NONE
hi Conceal
hi Cursor guibg=#fcbf49 ctermbg=215 gui=NONE cterm=NONE
hi CursorColumn guifg=NONE ctermfg=NONE guibg=#cad2c5 ctermbg=251 gui=NONE cterm=NONE
hi CursorLine guifg=NONE ctermfg=NONE guibg=#cad2c5 ctermbg=251 gui=NONE cterm=NONE
hi Directory guifg=#cad2c5 ctermfg=251 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi DiffAdd guifg=NONE ctermfg=NONE guibg=#02c39a ctermbg=36 gui=NONE cterm=NONE
hi DiffChange guifg=NONE ctermfg=NONE guibg=#f77f00 ctermbg=208 gui=NONE cterm=NONE
hi DiffDelete guifg=NONE ctermfg=NONE guibg=#d62828 ctermbg=160 gui=NONE cterm=NONE
hi DiffText guifg=NONE ctermfg=NONE guibg=#02c39a ctermbg=36 gui=NONE cterm=NONE
hi ErrorMsg guifg=#d62828 ctermfg=160 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi VertSplit guifg=#f1faee ctermfg=255 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Folded guifg=#1d0f0b ctermfg=233 guibg=#a6a7a5 ctermbg=248 gui=NONE cterm=NONE
hi FoldColumn guifg=#1d0f0b ctermfg=233 guibg=#a6a7a5 ctermbg=248 gui=NONE cterm=NONE
hi SignColumn guifg=#1d0f0b ctermfg=233 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi IncSearch guifg=#a6a7a5 ctermfg=248 guibg=#fcbf49 ctermbg=215 gui=NONE cterm=NONE
hi LineNr guifg=#cad2c5 ctermfg=251 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi MatchParen guifg=#fcbf49 ctermfg=215 guibg=NONE ctermbg=NONE gui=Bold cterm=Bold
hi Normal guifg=NONE ctermfg=NONE guibg=#a6a7a5 ctermbg=248 gui=NONE cterm=NONE
hi PMenu guifg=#1d0f0b ctermfg=233 guibg=#a6a7a5 ctermbg=248 gui=NONE cterm=NONE
hi PMenuSel guifg=#cad2c5 ctermfg=251 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Visual guifg=#1d0f0b ctermfg=233 guibg=#cad2c5 ctermbg=251 gui=NONE cterm=NONE
hi WarningMsg guifg=#f77f00 ctermfg=208 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Comment guifg=#02c39a ctermfg=36 guibg=NONE ctermbg=NONE gui=Italic cterm=Italic
hi Constant guifg=#1d0f0b ctermfg=233 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi String guifg=#1d0f0b ctermfg=233 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Character guifg=#1d0f0b ctermfg=233 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Boolean guifg=#1d0f0b ctermfg=233 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Number guifg=#f77f00 ctermfg=208 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Float guifg=#f77f00 ctermfg=208 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Identifier guifg=#1d0f0b ctermfg=233 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Function guifg=#333533 ctermfg=236 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Statement guifg=#1d0f0b ctermfg=233 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Conditional guifg=#1d0f0b ctermfg=233 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Repeat guifg=#1d0f0b ctermfg=233 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Label guifg=#1d0f0b ctermfg=233 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Operator guifg=#1d0f0b ctermfg=233 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Keyword guifg=#1d0f0b ctermfg=233 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Exception guifg=#1d0f0b ctermfg=233 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi PreProc guifg=#1d0f0b ctermfg=233 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Include guifg=#1d0f0b ctermfg=233 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Define guifg=#1d0f0b ctermfg=233 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Macro guifg=#1d0f0b ctermfg=233 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi PreCondit guifg=#1d0f0b ctermfg=233 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Type guifg=#333533 ctermfg=236 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi StorageClass guifg=#1d0f0b ctermfg=233 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Structure guifg=#1d0f0b ctermfg=233 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Typedef guifg=#1d0f0b ctermfg=233 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Special guifg=#1d0f0b ctermfg=233 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi SpecialChar guifg=#1d0f0b ctermfg=233 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Tag guifg=#1d0f0b ctermfg=233 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Delimiter guifg=#f77f00 ctermfg=208 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi SpecialComment guifg=#02c39a ctermfg=36 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Debug guifg=#1d0f0b ctermfg=233 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Underlined guifg=#333533 ctermfg=236 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Ignore guifg=#1d0f0b ctermfg=233 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Error guifg=#d62828 ctermfg=160 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Todo guifg=#fcbf49 ctermfg=215 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE

if exists('*term_setansicolors')
  let g:terminal_ansi_colors = repeat([0], 16)

endif

if has('nvim')
endif

" ===================================
" Generated by Estilo 1.5.0
" https://github.com/jacoborus/estilo
" ===================================

