" Specify a directory for plugins
" - For Neovim: stdpath('data') . '/plugged' Avoid using standard Vim directory names like 'plugin'
call plug#begin('~/.config/nvim/plugged/')

" Make sure you use single quotes

Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }
Plug 'lambdalisue/fern.vim'

Plug 'mhinz/vim-startify'
Plug 'antoinemadec/FixCursorHold.nvim'
Plug 'editorconfig/editorconfig-vim'

Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
Plug 'mhinz/vim-grepper', { 'on': ['Grepper', '<plug>(GrepperOperator)'] }

Plug 'puremourning/vimspector'
Plug 'dense-analysis/ale'
Plug 'SirVer/ultisnips'
Plug 'honza/vim-snippets'
Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
Plug 'autozimu/LanguageClient-neovim', {
    \ 'branch': 'next',
    \ 'do': 'bash install.sh',
    \ }

Plug 'dag/vim-fish'
Plug 'pangloss/vim-javascript'
Plug 'maxmellon/vim-jsx-pretty'
Plug 'leafgarland/typescript-vim'
Plug 'evanleck/vim-svelte'
Plug 'othree/html5.vim'
Plug 'amadeus/vim-mjml'
Plug 'wlangstroth/vim-racket'
Plug 'posva/vim-vue'
Plug 'neovimhaskell/haskell-vim'
Plug 'elixir-editors/vim-elixir'
Plug 'iamcco/markdown-preview.nvim', { 'do': 'cd app & yarn install'  }
Plug 'mattn/emmet-vim'
Plug 'cespare/vim-toml'
Plug 'jonsmithers/vim-html-template-literals'
Plug 'mustache/vim-mustache-handlebars'
Plug 'Glench/Vim-Jinja2-Syntax'
Plug 'uki00a/vim-deno'
Plug 'isobit/vim-caddyfile'
Plug 'Olical/conjure', {'tag': 'v4.13.0'}
Plug 'rust-lang/rust.vim'
Plug 'ziglang/zig.vim'
Plug 'lervag/vimtex'

Plug 'easymotion/vim-easymotion'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-repeat'
Plug 'jiangmiao/auto-pairs'
Plug 'tomtom/tcomment_vim'
Plug 'ap/vim-css-color'

Plug 'tpope/vim-fugitive'
Plug 'airblade/vim-gitgutter'

" COLORSCHEMES
Plug 'NLKNguyen/papercolor-theme'
Plug 'arzg/vim-colors-xcode'
Plug 'morhetz/gruvbox'
Plug 'atahabaki/archman-vim'
Plug 'doums/darcula'
Plug 'vim-scripts/Visual-Studio'
Plug 'jaredgorski/fogbell.vim'
Plug 'ntk148v/vim-horizon'
Plug 'yasukotelin/shirotelin'
Plug 'scolsen/hara'
Plug 'skbolton/embark'
Plug 'habamax/vim-colors-defminus'
Plug 'franbach/miramare'
Plug 'owickstrom/vim-colors-paramount'
Plug 'flrnd/candid.vim'
Plug 'reedes/vim-colors-pencil'
Plug 'fenetikm/falcon'
Plug 'chiendo97/intellij.vim'
Plug 'j201/stainless'
Plug 'nanotech/jellybeans.vim'
Plug 'romgrk/github-light.vim'
Plug 'dracula/vim', { 'as': 'dracula' }
Plug 'humanoid-colors/vim-humanoid-colorscheme'
Plug 'jsit/toast.vim'
Plug 'ajgrf/parchment'
Plug 'zefei/cake16'
Plug 'tyrannicaltoucan/vim-deep-space'
Plug 'sonph/onehalf', { 'rtp': 'vim/' }
Plug 'senran101604/neotrix.vim'
Plug 'rakr/vim-one'
Plug 'rakr/vim-two-firewatch'
Plug 'chase/focuspoint-vim'

" Initialize plugin system
call plug#end()

" =================== FUNCTIONS ===========================

function SetLSPShortcuts()
  nnoremap <leader>d :call LanguageClient#textDocument_definition()<CR>
  nnoremap <leader>lr :call LanguageClient#textDocument_rename()<CR>
  nnoremap <leader>lf :call LanguageClient#textDocument_formatting()<CR>
  nnoremap <leader>lt :call LanguageClient#textDocument_typeDefinition()<CR>
  nnoremap <leader>lx :call LanguageClient#textDocument_references()<CR>
  nnoremap <leader>la :call LanguageClient_workspace_applyEdit()<CR>
  nnoremap <leader>lc :call LanguageClient#textDocument_completion()<CR>
  nnoremap <leader>lh :call LanguageClient#textDocument_hover()<CR>
  nnoremap <leader>ls :call LanguageClient_textDocument_documentSymbol()<CR>
  nnoremap <leader>lm :call LanguageClient_contextMenu()<CR>
endfunction()

call SetLSPShortcuts()

function! s:init_fern() abort
  " Define NERDTree like mappings
  nmap <buffer> o <Plug>(fern-action-open:edit)
  nmap <buffer> go <Plug>(fern-action-open:edit)<C-w>p
  nmap <buffer> t <Plug>(fern-action-open:tabedit)
  nmap <buffer> T <Plug>(fern-action-open:tabedit)gT
  nmap <buffer> i <Plug>(fern-action-open:split)
  nmap <buffer> gi <Plug>(fern-action-open:split)<C-w>p
  nmap <buffer> s <Plug>(fern-action-open:vsplit)
  nmap <buffer> gs <Plug>(fern-action-open:vsplit)<C-w>p
  nmap <buffer> ma <Plug>(fern-action-new-path)
  nmap <buffer> d <Plug>(fern-action-remove)
  nmap <buffer> P gg

  nmap <buffer> C <Plug>(fern-action-enter)
  nmap <buffer> u <Plug>(fern-action-leave)
  nmap <buffer> r <Plug>(fern-action-reload)
  nmap <buffer> R gg<Plug>(fern-action-reload)<C-o>
  nmap <buffer> cd <Plug>(fern-action-cd)
  nmap <buffer> CD gg<Plug>(fern-action-cd)<C-o>

  nmap <buffer> I <Plug>(fern-action-hide-toggle)

  nmap <buffer> q :<C-u>quit<CR>

  nmap <buffer><expr>
      \ <Plug>(fern-my-expand-or-collapse)
      \ fern#smart#leaf(
      \   "\<Plug>(fern-action-collapse)",
      \   "\<Plug>(fern-action-expand)",
      \   "\<Plug>(fern-action-collapse)",
      \ )

  nmap <buffer><nowait> l <Plug>(fern-my-expand-or-collapse)
endfunction

" golang templates
function DetectGoHtmlTmpl()
    if expand('%:e') == "html" && search("{{") != 0
        set filetype=gohtmltmpl
    endif
endfunction

" ============== KEYBINDINGS ===========================
inoremap <special> jk <ESC>
map <C-n> :Fern . -drawer<CR>
" map <C-M-n> :NERDTreeFocus<CR>
map <C-p> :FZF<CR>
map <leader>a :edit ~/.config/nvim/init.vim<CR>

" visualmode fix
xnoremap < <gv
xnoremap > >gv
vnoremap // y/\V<C-R>=escape(@",'/\')<CR><CR>

" To save, ctrl-s.
nmap <c-s> :w<CR>
imap <c-s> <Esc>:w<CR>a

nmap <leader>f :ALEFix<CR>

nnoremap <leader>g :Grepper -tool rg<cr>

" ================ CONFIGURATIONS ====================
let g:deoplete#enable_at_startup = 1

let g:htl_css_templates = 1

let g:mkdp_auto_close = 0

let g:AutoPairsShortcutFastWrap='<C-e>'
let g:mkdp_auto_close = 1

let g:vue_pre_processors = []

let g:UltiSnipsExpandTrigger = '<tab>'

let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"

let g:ale_set_highlights = 0

let g:vimspector_enable_mappings = 'HUMAN'

" @see https://github.com/autozimu/LanguageClient-neovim/issues/1191
" let g:LanguageClient_diagnosticsDisplay = {
"     \     1: {
"       \      "name": "Error",
"       \      "texthl": "ALEError",
"       \      "signText": "✖",
"       \      "virtualTexthl": "Error",
"       \  },
"       \  2: {
"       \      "name": "Warning",
"       \      "texthl": "ALEWarning",
"       \      "signText": "⚠",
"       \      "virtualTexthl": "Todo",
"       \  },
"       \  3: {
"       \      "name": "Information",
"       \      "texthl": "ALEInfo",
"       \      "signText": "ℹ",
"       \      "virtualTexthl": "Todo",
"       \  },
"       \  4: {
"       \      "name": "Hint",
"       \      "texthl": "ALEInfo",
"       \      "signText": "➤",
"       \      "virtualTexthl": "Todo",
"       \  },
"       \}

let g:ale_linter_aliases = {
\   'svelte': ['javascript', 'css']
\}

let g:ale_linters = {
      \ 'javascript': ['eslint'],
      \ 'typescript': ['eslint'],
      \ 'javascript.jsx': ['eslint'],
      \ 'typescript.tsx': ['eslint'],
      \ 'go': ['gofmt'],
      \ 'svelte': ['eslint'],
      \ 'vue': ['eslint'],
      \ 'css': ['stylelint'],
      \ 'scss': ['stylelint']
      \}

let g:ale_fixers = {
\   '*': ['remove_trailing_lines', 'trim_whitespace'],
\   'javascript': ['prettier','eslint'],
\   'typescript': ['prettier','eslint'],
\   'javascript.jsx': ['prettier','eslint'],
\   'typescript.tsx': ['prettier','eslint'],
\   'svelte': ['prettier','eslint'],
\   'vue': ['prettier','eslint'],
\   'css': ['stylelint'],
\   'scss': ['stylelint']
\}

let g:LanguageClient_serverCommands = {
    \ 'javascript': ['typescript-language-server', '--tsserver-log-file', '~/.logs/tsserver-log.txt', '--stdio'],
    \ 'javascript.jsx': ['typescript-language-server', '--tsserver-log-file', '~/.logs/tsserver-log.txt', '--stdio'],
    \ 'javascriptreact': ['typescript-language-server', '--tsserver-log-file', '~/.logs/tsserver-log.txt', '--stdio'],
    \ 'typescript': ['typescript-language-server', '--tsserver-log-file', '~/.logs/tsserver-log.txt', '--stdio'],
    \ 'typescript.tsx': ['typescript-language-server', '--tsserver-log-file', '~/.logs/tsserver-log.txt', '--stdio'],
    \ 'typescriptreact': ['typescript-language-server', '--tsserver-log-file', '~/.logs/tsserver-log.txt', '--stdio'],
    \ 'go': ['gopls'],
    \ 'c': ['ccls'],
    \ 'cpp': ['ccls'],
    \ 'svelte': ['~/Programs/language-tools/packages/language-server/bin/server.js', '--stdio'],
    \ 'elixir': ['~/Documents/code/elixir/elixir-ls/release/language_server.sh', '--stdio'],
    \ 'vue': ['vls'],
    \ 'rust': ['rustup', 'run', 'stable', 'rls'],
    \ 'zig': ['~/Programs/zls/zig-cache/bin/zls'],
    \ }

" let g:asyncomplete_auto_popup = 0
let g:lsp_diagnostics_enabled = 1        " disable diagnostics support


"========================= SETTINGS =======================
set t_Co=256
set termguicolors
set showcmd
set hidden

set tabstop=4
set softtabstop=4
set shiftwidth=4
set expandtab

set autoindent
set smartindent

set encoding=utf-8

set nohidden
set autoread
set ruler
set hlsearch
set incsearch
set ignorecase
set smartcase
set wrapscan
set wildmenu
set lazyredraw

" Folding
set foldmethod=marker
set foldlevel=0
set modelines=1

set magic
set number
set relativenumber
set mouse=c
set noswapfile
set nobackup
set nowritebackup
set list
set secure
set fileformat=unix
set path+=**
"set clipboard=unnamedplus
set t_Co=256
set backupcopy=yes
set colorcolumn=100

set shortmess+=c   " Shut off completion messages
set belloff+=ctrlg " If Vim beeps during completion

set formatoptions-=o    " O and o, don't continue comments

set scrolloff=10     " Make it so there are always ten lines below my cursor

if &shell =~# 'fish$'
    set shell=bash
endif

"===================== SYNTAX =================================
syntax on

let g:candid_color_store = {
    \ "grey": {"gui": "#6c757d", "cterm256": "244"},
    \}
set background=light
colorscheme intellij

"cake16 fix
" hi MatchParen guifg=13 guibg=#afafaf

" two-firewatch dark
hi MatchParen guifg=#afafaf guibg=13

" deep space fix
" hi MatchParen guifg=#dddddd guibg=#a91b60

" horizon fix
" hi PMenu guibg=#cccccc guibg=#777777

" xcodelight fix
" hi MatchParen guifg=#2233df guibg=#cccccc guisp=NONE gui=NONE cterm=NONE

" fogbell_light fix
"hi Visual  guifg=#1C1C1C guibg=#B0B0B0 gui=none
"hi CursorLine  guifg=#1C1C1C guibg=#B0B0B0 gui=none
"hi CursorLineNr  guifg=#1C1C1C guibg=#B0B0B0 gui=none

" miramare fix
" hi MatchParen guifg=#233523 guibg=#956832

" ====================== FILE TYPE SETTINGS ===============================
autocmd BufNewFile,BufFilePre,BufRead *.md set filetype=markdown
filetype plugin on
filetype indent on

autocmd BufNewFile,BufFilePre,BufRead *.ejs set filetype=html
autocmd BufNewFile,BufFilePre,BufRead Dockerfile* set filetype=dockerfile
autocmd BufNewFile,BufFilePre,BufRead Caddyfile* set filetype=caddyfile

autocmd BufNewFile,BufFilePre,BufRead *.yml set tabstop=2
autocmd BufNewFile,BufFilePre,BufRead *.yml set softtabstop=2
autocmd BufNewFile,BufFilePre,BufRead *.yml set shiftwidth=2

augroup filetypedetect
    au! BufRead,BufNewFile * call DetectGoHtmlTmpl()
augroup END

augroup fern-custom
  autocmd! *
  autocmd FileType fern call s:init_fern()
augroup END


" ===================== COMMANDS =========================
command! Todo :Grepper -tool rg -query TODO\|FIXME


"========= ASYNC COMPLETION =============

" inoremap <expr> <Tab>   pumvisible() ? "\<C-n>" : "\<Tab>"
" inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"
" inoremap <expr> <cr>    pumvisible() ? "\<C-y>" : "\<cr>"
