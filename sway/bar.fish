#!/usr/bin/env fish

set today (date "+%a %F %H:%M")

set battery (cat /sys/class/power_supply/BAT1/capacity)

set wifi_name (nmcli -f general.connection device show wlan0 | awk '{print $2}')

echo '' $wifi_name "|" "" $battery "|" "" $today
