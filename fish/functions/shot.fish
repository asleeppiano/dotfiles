set SCREENSHOTDIR ~/Screenshots

set TIME (date +%Y-%m-%d-%H-%M-%S)
set IMGPATH $SCREENSHOTDIR/screenshot-$TIME.png

function shot
    echo $IMGPATH
    grim -g (slurp) $IMGPATH
    notify-send "screenshotted!" -t 3000
end

