# disable ctrl-s freeze. remap ctrl-s
# outputs error => stty: 'standard input': Inappropriate ioctl for device
# if device is not a terminal. Example vim or emacs
# stty -ixon
# bell off
xset b off

set -g -x EDITOR "nvim"
set -g -x BROWSER "firefox"
set -g -x MOZ_ENABLE_WAYLAND 1
set -g -x MANPATH "/usr/share/man"

function ll
    exa -lh $argv
end

set CURRENT_PROJECT_PATH_FILE "~/.config/fourtyone11/currentprojectpath"
set MY_SITE_PATH "~/Documents/code/my-site"
set WORK_PATH "~/Documents/koshelek_work"
set SICP_PATH "~/Documents/code/sicp"


######## npm ###################
set NPM_PACKAGES "$HOME/.npm-packages"
set PATH $PATH $NPM_PACKAGES/bin
set MANPATH $NPM_PACKAGES/share/man $MANPATH
#####################################

alias nvc="nvim ~/.config/nvim/init.vim"
alias fishc="nvim ~/.config/fish/config.fish"
alias attyc="nvim ~/.config/alacritty/alacritty.yml"
alias dev="cd (cat $CURRENT_PROJECT_PATH_FILE) && tmux"
alias site="cd $MY_SITE_PATH && tmux new 'nvim'"
alias work="cd $WORK_PATH"
alias sicp="cd $SICP_PATH && tmux new 'nvim'"

function fish_greeting
  salute printall
end

bind \cq beginning-of-line

starship init fish | source
